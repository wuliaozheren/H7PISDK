#include "app_config.h"

#ifdef app_bc20

osThreadId app_handler_bc20;
osThreadId app_handler_led;
osThreadId app_handler_usb2uart;

/*********************************************************************
*
*
*
**********************************************************************/
void app_led_task(void const * argument)
{
	while(1)
	{
		led(1,0);led(2,1);
		osDelay(500);
		led(1,1);led(2,0);
		osDelay(500);
	}
}

void app_bc20_task(void const * argument)
{
	int i = 0;
	int len = 0;
	char* cmd=NULL;
	char gnss;
	bc20_Init();
	
	while(1)
	{
		if(cdc.rxLength > 0)
		{
			cdc.rxNew2Uart = 0;
			uart_send(cdc.rxBuffer,cdc.rxLength);
			cdc_clear_buffer();
		}
		//
		//gnss
		bc20_SetGNSS("1");
		osDelay(2000);
		bc20_GetGNSS(&gnss);
	}
}

void app_usb2uart_task(void const * argument)
{
	while(1)
	{
		if(uart.rxNew2Usb == 1 && uart.rxLength > 0)
		{		
			uart.rxNew2Usb = 0;
			cdc_send(uart.rxBuffer,uart.rxLength);
			//uart_clear_buffer();
		}
	}
}
/*********************************************************************


**********************************************************************/
void app_bc20_start(void)
{	

  osThreadDef(led_task, app_led_task, osPriorityNormal, 1, 1024);
	app_handler_led = osThreadCreate(osThread(led_task), NULL);
	
  osThreadDef(bc20_task, app_bc20_task, osPriorityHigh, 2,4096);
	app_handler_bc20 = osThreadCreate(osThread(bc20_task), NULL);
	
  osThreadDef(usb2uart_task, app_usb2uart_task, osPriorityHigh, 3,4096);
	app_handler_usb2uart = osThreadCreate(osThread(usb2uart_task), NULL);
}

#endif