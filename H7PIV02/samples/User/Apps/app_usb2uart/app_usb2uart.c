#include "app_config.h"

#ifdef app_usb2uart

osThreadId app_handler_usb2uart;
osThreadId app_handler_led;

/*********************************************************************


**********************************************************************/

void app_led_task(void const * argument)
{
	while(1)
	{
		led(1,0);led(2,1);
		osDelay(500);
		led(1,1);led(2,0);
		osDelay(500);
	}
}


void cdc_cli_uart2usb(void)
{
	if(uart.rxNew2Usb == 1 && uart.rxLength > 0)
	{		
		uart.rxNew2Usb = 0;
		cdc_send(uart.rxBuffer,uart.rxLength);
		uart_clear_buffer();
	}
}

void cdc_cli_usb2uart(void)
{	
	int i = 0;
	int len = 0;
	char* cmd=NULL;
	
	if(cdc.rxLength > 0)
	{
		cdc.rxNew2Uart = 0;
		uart_send(cdc.rxBuffer,cdc.rxLength);
		cdc_clear_buffer();
	}
}

void app_usb2uart_task(void const * argument)
{
	while(1)
	{
		cdc_cli_usb2uart();
		cdc_cli_uart2usb();
		osDelay(1);
	}
}

/*********************************************************************


**********************************************************************/
void app_usb2uart_start(void)
{	

  osThreadDef(led_task, app_led_task, osPriorityNormal, 1, 1024);
	app_handler_led = osThreadCreate(osThread(led_task), NULL);
	
  osThreadDef(usb2uart_task, app_usb2uart_task, osPriorityHigh, 2,5120);
	app_handler_usb2uart = osThreadCreate(osThread(usb2uart_task), NULL);
	
}

#endif