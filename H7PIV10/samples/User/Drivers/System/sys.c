#include "sys.h"

uint8_t   sys_inited = 0;
uint32_t  sys_dev_num = 0;
sys_dev_t sys_dev_array[SYS_DEV_ARRAY_SIZE]={SYS_DEV_TYPE_NUM,NULL};
/******************************************************************
 * 
 * regist device to sys
 * 
 * ***************************************************************/
void sys_device_register(void* dev, sys_dev_type_t type)
{
	if(type < SYS_DEV_TYPE_NUM && type >= 0)
	{
		sys_dev_array[sys_dev_num].type = type;
		sys_dev_array[sys_dev_num].device = dev;
		sys_dev_num++;
	}
}

/******************************************************************

全局变量


******************************************************************/
DriversStatus_t drivers_status;
/******************************************************************

system init


******************************************************************/
void sys_Init(void)
{
	FRESULT res = 0;
	BYTE work[_MAX_SS];
	//
	//onboard peripheral
	//	
	W25Q_SPI_Init();
	LY68L_SPI_Init();
	drivers_status.psram = (DriversStatus)LY68L_SPI_Test();
	//
	//file system init
	//
  retSD = FATFS_LinkDriver(&SD_Driver, SDPath);
  retUSER = FATFS_LinkDriver(&USER_Driver, USERPath);
	//
	//mount spi flash file system
	//
	res = f_mount(&USERFatFS, (TCHAR const*)USERPath, 1);
	if(res != FR_OK)
	{
		if(res == FR_NO_FILESYSTEM || res == FR_DISK_ERR)
		{
			res = f_mkfs(USERPath, FM_ANY ,0 , work, sizeof work);
		}
		if(res == FR_OK)
		{
			if(f_mount(&USERFatFS, (TCHAR const*)USERPath, 1)==FR_OK)
			{
				drivers_status.spiflash = DRIVERS_OK;
			}
			else
			{
				drivers_status.spiflash = DRIVERS_FAIL;
			}
		}
		else
		{
			drivers_status.spiflash = DRIVERS_FAIL;
		}
	}
	else
	{
		drivers_status.spiflash = DRIVERS_OK;
	}
	//
	//mount sd card file system
	//
	res = f_mount(&SDFatFS, (TCHAR const*)SDPath, 1);
	if(res!=FR_OK)
	{
		if(res == FR_NO_FILESYSTEM || res == FR_DISK_ERR)
		{
			res = f_mkfs(SDPath, FM_FAT32 ,0 , work, sizeof work);
		}
		if(res == FR_OK)
		{
			if(f_mount(&SDFatFS, (TCHAR const*)SDPath, 1)==FR_OK)
			{
				drivers_status.sdcard = DRIVERS_OK;
			}
			else
			{
				drivers_status.sdcard = DRIVERS_FAIL;
			}
		}
		else
		{
			drivers_status.sdcard = DRIVERS_FAIL;
		}
	}
	else
	{
		drivers_status.sdcard = DRIVERS_OK;
	}
	//
	//usb init
	//
  MX_USB_DEVICE_Init();
	drivers_status.usb = DRIVERS_OK;
	//
	//device regist
}
/**********************************************
app start api
***********************************************/
void app_blink_start(void);
void app_testmotor_start(void);
void app_player_start(void);
void StartDefaultTask(void const * argument);
/******************************************************************
//
//系统启动
//
******************************************************************/
osThreadId defaultTaskHandle;
void sys_OsStart(void)
{
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 20480);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
	//
	//OS start
	osKernelStart();
}



/******************************************************************
//
//默认任务
//
******************************************************************/

void StartDefaultTask(void const * argument)
{
	//
	//sys init
	sys_Init();
	sys_inited = 1;
	//
	//APP definition
	app_blink_start();
	//
	//background thread;
	while(1)
	{
		#if (USB_STACK_TYPE==USB_STACK_TYPE_TEENY)
		int tusb_composite_task(void);
		tusb_composite_task();
		#elif (USB_STACK_TYPE==USB_STACK_TYPE_ST)
		osDelay(10000);
		#endif
	}

}