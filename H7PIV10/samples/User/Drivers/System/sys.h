#ifndef __SYS_H__
#define __SYS_H__

#include "stm32h7xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"



#include "drivers_config.h"
/*****************************************
 on board drivers
*****************************************/
#include "led.h"
#include "spiflash.h"
#include "psram.h"
#include "qspiflash.h"
#include "cdc.h"
#include "io.h"
#include "cJSON.h"
#include "fifo.h"
#include "fatfs.h"
/******************************************
hardware device drivers
******************************************/
#include "rz7899.h"

#include "player_dev.h"
#include "recorder_dev.h"

#include "player_dac.h"
#include "recorder_adc.h"
/******************************************
software device drivers
******************************************/
#include "wavplayer.h"


/*********************************************************
 * 
 * drivers status
 * 
 * *******************************************************/
typedef enum
{
	DRIVERS_FAIL = 0,
	DRIVERS_OK,
	DRIVERS_STATUS_NUM
	
}DriversStatus;

/*********************************************************
 * 
 * on board drivers status
 * 
 * *******************************************************/
typedef struct
{
	DriversStatus sdcard;
	DriversStatus gui;
	DriversStatus usb;
	DriversStatus uart;
	DriversStatus spiflash;
	DriversStatus psram;
	
}DriversStatus_t;

/*********************************************************
 * 
 * system device type definition
 * 
 * *******************************************************/
typedef enum
{
	SYS_DEV_PLAYER = 0,
	SYS_DEV_RECORDER,
	SYS_DEV_TYPE_NUM

}sys_dev_type_t;
/*********************************************************
 * 
 * system device definition
 * 
 * *******************************************************/
typedef struct
{
	sys_dev_type_t type;
	void*          device;
}sys_dev_t;

#define SYS_DEV_ARRAY_SIZE 128

/*******************************************************
 * file system parameter
 * 
 * ****************************************************/
extern uint8_t retSD; /* Return value for SD */
extern char SDPath[4]; /* SD logical drive path */
extern FATFS SDFatFS; /* File system object for SD logical drive */
extern FIL SDFile; /* File object for SD */
extern uint8_t retUSER; /* Return value for USER */
extern char USERPath[4]; /* USER logical drive path */
extern FATFS USERFatFS; /* File system object for USER logical drive */
extern FIL USERFile; /* File object for USER */
/*******************************************************
 * driver status
 * 
 * ****************************************************/
extern DriversStatus_t drivers_status;
/*******************************************************
 * sys dev
 * 
 * ****************************************************/
void   sys_device_register(void* dev, sys_dev_type_t type);
void   MX_USB_DEVICE_Init(void);
/*******************************************************
 * device added here
 * 
 * ****************************************************/
extern player_dev_t player_dac;
/*******************************************************
 * os task defined
 * 
 * ****************************************************/
extern osThreadId   defaultTaskHandle;



void   sys_OsStart(void);



#endif

