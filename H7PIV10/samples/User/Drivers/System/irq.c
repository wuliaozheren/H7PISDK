
#include "irq.h"

extern  DAC_HandleTypeDef hdac1;
extern  SPI_HandleTypeDef hspi2;

//extern  I2C_HandleTypeDef hi2c2;
//extern  I2C_HandleTypeDef hi2c1;

//uint8_t i2c2_tx_ok = 0;
//uint8_t i2c2_rx_ok = 0;

//uint8_t i2c1_tx_ok = 0;
//uint8_t i2c1_rx_ok = 0;

uint8_t spi2_txend = 0;
uint8_t spi2_rxend = 0;
/***************************************************************
 * 
 * 
 * spi TxCpltCallback
 * 
 * ************************************************************/
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi == &hspi2)
	{
		spi2_txend = 1;
	}
}
/***************************************************************
 * 
 * 
 * spi RxCpltCallback
 * 
 * ************************************************************/
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi == &hspi2)
	{
		spi2_rxend = 1;
	}
}


/***************************************************************
 * 
 * 
 * DAC channel2 convert complete callback
 * 
 * ************************************************************/
void HAL_DACEx_ConvCpltCallbackCh2(DAC_HandleTypeDef* hdac)
{
	if(hdac == &hdac1)
	{
		player_dac_irq_conv_complete();
	}
}




//void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
//{
//	if(hi2c == &hi2c2)
//	{
//		i2c2_tx_ok = 1;
//	}
//	else if(hi2c == &hi2c1)
//	{
//		
//	}
//}



//void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
//{
//	if(hi2c == &hi2c2)
//	{
//		i2c2_rx_ok = 1;
//	}
//	else if(hi2c == &hi2c1)
//	{
//		
//	}
//}

