#include "player_dac.h"
#include "string.h"

player_dac_dev_t player_dac_dev;


/*****************************************************
*
*
*   playr_dac_struct data init
*
*****************************************************/
void player_dac_data_init(void)
{
	memset(player_dac_dev.buffer0,0,sizeof(PLAYER_BUFFER_SIZE));
	memset(player_dac_dev.buffer1,0,sizeof(PLAYER_BUFFER_SIZE));
	
	player_dac_dev.buffer_switch[0] =  player_dac_dev.buffer0;
	player_dac_dev.buffer_switch[1] =  player_dac_dev.buffer1;
	
	player_dac_dev.buffer_valid[0] = 0;
	player_dac_dev.buffer_valid[1] = 0;
	player_dac_dev.buffer_index = 0;
	player_dac_dev.status = PLAYER_STOP;
	
}

/*****************************************************
*
*
*   playr_dac_init(void)
*
*****************************************************/
void player_dac_dac_init(void)
{
	//dac init has been finished in main function
}
/*****************************************************
*
*
*   playr_timer_init(void)
*   default for 48kHz sample rate
*
*****************************************************/
void player_dac_timer_init(void)
{
	//timer basical init has been finish in main function
	
}

/*****************************************************
*
*
*   playr_deinit(void)
*
*****************************************************/
void player_dac_deinit(void)
{
	player_dac_data_init();
	player_dac_timer_init();
}



/****************************************************
 * 
 * player get status
 * 
 * **************************************************/
player_dev_status_t player_dac_get_status(void)
{
	return player_dac_dev.status;
}
/*****************************************************
 * 
 * 
 * set player sample rate
 * 
 * 
 * ***************************************************/
uint8_t player_dac_set_samplerate(uint32_t rate)
{
	uint32_t counter;
	uint32_t freq;
//	freq = PLAYER_TIMER_FREQ/PLAYER_TIMER_PRESCALER;
//	counter = freq/rate;
//	__HAL_TIM_SetCounter(PLAYER_TIMER_HANDLE,counter-1);
//	__HAL_TIM_SetAutoreload(PLAYER_TIMER_HANDLE,counter/2-1);

	return 1;
}

/*****************************************************
 * 
 * set player buffer status
 * @param buffer
 * buffer address pointer
 * @return
 * 0:    invalid
 * 1:    valid
 * 0xff: err
 * 
 * ***************************************************/
uint8_t player_dac_set_buffer_valid(uint8_t* buffer, uint8_t state)
{
	if(buffer == player_dac_dev.buffer0)
	{
		player_dac_dev.buffer_valid[0] = state;
		return player_dac_dev.buffer_valid[0];
	}
	else if(buffer == player_dac_dev.buffer1)
	{
		player_dac_dev.buffer_valid[1] = state;
		return player_dac_dev.buffer_valid[1];
	}
	else
	{
		return 0xff;
	}
}
/*****************************************************
 * 
 * player stop
 * 
 * ***************************************************/
uint8_t player_dac_stop(void)
{
	uint8_t res = 0;
	//
	//HAL_OK(value: 0) will be return by HAL api if run successfully
	res += HAL_TIM_Base_Stop(PLAYER_TIMER_HANDLE);
	res += HAL_DAC_Stop_DMA(PLAYER_DAC_HANDLER, PLAYER_CH_LEFT); 
//	if( res <= 1)
//	{
	player_dac_data_init();
	player_dac_dev.status = PLAYER_STOP;
//	}
	return res;
}
/*****************************************************
 * 
 * player start
 * 
 * ***************************************************/
uint8_t player_dac_start(void)
{
	uint8_t res = 0;
	player_dac_dev.status = PLAYER_START;
	//
	//HAL_OK(value: 0) will be return by HAL api if run successfully
	if(player_dac_dev.buffer_valid[player_dac_dev.buffer_index])
	{
		res += HAL_TIM_Base_Start(PLAYER_TIMER_HANDLE);
		
		res += HAL_DAC_Start_DMA(PLAYER_DAC_HANDLER, PLAYER_CH_LEFT, (uint32_t*)(player_dac_dev.buffer_switch[player_dac_dev.buffer_index]), PLAYER_BUFFER_SIZE, DAC_ALIGN_8B_R);
		if (res == 0)
		{
			player_dac_dev.status = PLAYER_PLAYING;
		}
	}
	return !res;
}

/*****************************************************
 * 
 * player get buffer address
 * return the address of buffer
 * 
 * ***************************************************/
uint8_t* player_dac_get_buffer_pos(void)
{
	if(player_dac_dev.status == PLAYER_STOP && player_dac_dev.buffer_valid[0]==0)
	{
		return player_dac_dev.buffer0;
	}
	else if(player_dac_dev.buffer_index == 0 && player_dac_dev.status == PLAYER_PLAYING && player_dac_dev.buffer_valid[1]==0)
	{
		return player_dac_dev.buffer1;
	}
	else if(player_dac_dev.buffer_index == 1 && player_dac_dev.status == PLAYER_PLAYING && player_dac_dev.buffer_valid[0]==0)
	{
		return player_dac_dev.buffer0;
	}
	return NULL;
}



/*****************************************************
 * 
 * 
 * stm32 irq callback,
 * 
 * 
 * **************************************************/
void player_dac_irq_conv_complete(void)
{
	HAL_TIM_Base_Stop(PLAYER_TIMER_HANDLE);
	player_dac_dev.status = PLAYER_PAUSE;
	//
	//clear buffer
	memset(player_dac_dev.buffer_switch[player_dac_dev.buffer_index],0,PLAYER_BUFFER_SIZE);
	player_dac_dev.buffer_valid[player_dac_dev.buffer_index]=0;
	//
	//set to play the other buffer if valid.
	if(player_dac_dev.buffer_valid[(!player_dac_dev.buffer_index)]==1)
	{
		player_dac_dev.buffer_index = !player_dac_dev.buffer_index;
		player_dac_start();
	}
	else
	{
		player_dac_stop();
	}
}


void player_dac_shutdown(uint8_t sd)
{
	HAL_GPIO_WritePin(PLAYER_SD_PIN,sd);
}
/*****************************************************
 * 
 * 
 * device init
 * 
 * 
 * **************************************************/
void player_dev_dac_init(player_dev_t* dev)
{
	dev->delay_ms                = PLAYER_DELAY_MS;
	dev->start            = player_dac_start;
	dev->stop             = player_dac_stop;
	dev->set_samplerate   = player_dac_set_samplerate;
	dev->set_buffer_valid = player_dac_set_buffer_valid;
	dev->get_status       = player_dac_get_status;
	dev->get_buffer_pos   = player_dac_get_buffer_pos;
	dev->buffer_size      = PLAYER_BUFFER_SIZE;
	dev->deinit           = player_dac_deinit;
	dev->shutdown         = player_dac_shutdown;
	
	player_dac_dac_init();
	player_dac_data_init();
	player_dac_timer_init();
}