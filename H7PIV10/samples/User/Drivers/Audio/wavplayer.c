#include "wavplayer.h"
/*********************************************
 * 
 * device definition
 * 
 * ******************************************/
wavplayer_dev_t* wavplayer;
/*********************************************
 * 
 * player wave init
 * 
 * ******************************************/
void wavplayer_link(player_dev_t* player, recorder_dev_t* recorder)
{
	if(player!=NULL)
	{
		wavplayer->player = player;
	}
	if(recorder!=NULL)
	{
		wavplayer->recorder = recorder;
	}
}
/*********************************************
 * 
 * player wave deinit
 * 
 * ******************************************/
void wavplayer_unlink(void)
{
	wavplayer = NULL;
}

/*********************************************
 * 
 * read wav file info
 * @param:
 * data: data pointer 
 * idx: data index(position)
 * fmt: data fmt(endianness)
 * @return: data info
 * 
 * ******************************************/
uint32_t wavplayer_read_info(uint8_t *data,uint8_t idx,uint8_t length,Endianness_t fmt)
{
  	uint32_t index=0;
  	uint32_t temp=0;
  
  	for(index=0;index<length;index++)temp|=data[idx+index]<<(index*8);
  	if (fmt == BigEndian)temp= __REV(temp);
  	return temp;
}
/*********************************************
 * 
 * read wav file info
 * @parm:offset,origin data offset,use for
 * 		returning the data offset num;
 * @return: uint8_t,result
 * result > 0, some err occured
 * result = 0, all info parse successfully
 * 
 * ******************************************/
uint8_t wavplayer_parse(wav_format_t* fmt, uint8_t* data, uint32_t* offset)
{
  	uint32_t temp=0x00;
  	uint32_t extraformatbytes=0;

  	temp=wavplayer_read_info(data,0,4,BigEndian);//'RIFF'//读'RIFF'
  	if(temp!=CHUNK_ID)return 1;
  	fmt->RIFFchunksize=wavplayer_read_info(data,4,4,LittleEndian);////读文件长度
  	temp=wavplayer_read_info(data,8,4,BigEndian);//读'WAVE'
  	if(temp!=FILE_FORMAT)return 2;
  	temp=wavplayer_read_info(data,12,4,BigEndian);//读'fmt '
  	if(temp!=FORMAT_ID)return 3;
  	temp=wavplayer_read_info(data,16,4,LittleEndian);//读'fmt'数据长度
  	if(temp!=0x10)
	{
	  extraformatbytes=1;
	}
  	fmt->FormatTag=wavplayer_read_info(data,20,2,LittleEndian);//读音频格式
  	if(fmt->FormatTag!=WAVE_FORMAT_PCM)return 4;  
  	fmt->NumChannels=wavplayer_read_info(data,22,2,LittleEndian);//声道数 
	fmt->SampleRate=wavplayer_read_info(data,24,4,LittleEndian);//采样率
	fmt->ByteRate=wavplayer_read_info(data,28,4,LittleEndian);//波特率
	fmt->BlockAlign=wavplayer_read_info(data,32,2,LittleEndian);//读块对齐
	fmt->BitsPerSample=wavplayer_read_info(data,34,2,LittleEndian);//读采样点位数	
	if(fmt->BitsPerSample!=BITS_PER_SAMPLE_16)return 5;
	*offset=36;
	if(extraformatbytes==1)
	{
		temp=wavplayer_read_info(data,36,2,LittleEndian);//读额外格式字节
		if(temp!=0x00)return 6;
		temp=wavplayer_read_info(data,38,4,BigEndian);//读'fact'
		if(temp!=FACT_ID)return 7;
		temp=wavplayer_read_info(data,42,4,LittleEndian);//读Fact数据大小
		*offset+=10+temp;
	}
	temp=wavplayer_read_info(data,(*offset),4,BigEndian);//读'data'
	*offset+=4;
	if(temp!=DATA_ID)return 8;
	fmt->DataSize=wavplayer_read_info(data,(*offset),4,LittleEndian);//读采样数据大小
	*offset+=4;
	return 0;
}

/*********************************************
 * 
 * player wave stop
 * @return
 * stop player result
 * 
 * ******************************************/
uint8_t wavplayer_player_stop(void)
{
	return wavplayer->player->stop();
}

/*********************************************
 * 
 * player wave start
 * @param path:
 * sound file path
 * @return result:
 * 0-successful
 * 1-open/read file fail
 * 
 * ******************************************/
uint8_t wavplayer_player_start(char* path)
{
	FRESULT  res;
	UINT     br;
	uint8_t*     wav_buffer;
	uint32_t     wav_offset;
	FIL          wav_file;
	wav_format_t wav_fmt;
	uint32_t     length;
	int8_t*       temp_buffer;
	uint32_t      temp_length = 0;
	
	temp_buffer = malloc(wavplayer->player->buffer_size);
	//
	//open sound file
  res = f_open(&wav_file,(TCHAR*)path , FA_READ);
	if(res!=FR_OK)
	{
		free(temp_buffer);
		return 1;
	}
	//
	//get player device's buffer address
	wav_buffer = wavplayer->player->get_buffer_pos();
	//
	//read file and parse the file info and sound fmt
	res = f_read(&wav_file,wav_buffer, wavplayer->player->buffer_size, &br);
	if(res!=FR_OK)
	{
		free(temp_buffer);
		return 1;
	}
	//
	//parse the sound fmt
	while(wavplayer_parse(&wav_fmt,(uint8_t*)wav_buffer, &wav_offset))
	{
		wavplayer->player->delay_ms(1);
	}
	//
	//read real data
  res = f_lseek(&wav_file, wav_offset);// set real data position
	if(res!=FR_OK)
	{
		free(temp_buffer);
		return 1;
	}
	//
	//according to the fmt, reinit the dev
	wavplayer->player->set_samplerate(wav_fmt.SampleRate);
	//
	//8bit one channel
	if(wav_fmt.BitsPerSample == 8 && wav_fmt.NumChannels == 1)
	{
		do
		{
			wav_buffer = wavplayer->player->get_buffer_pos();
			if(wav_buffer == NULL)
			{
				wavplayer->player->delay_ms(1);
				continue;
			}
			res = f_read(&wav_file, wav_buffer, wavplayer->player->buffer_size, &br);
			if(res != FR_OK)
			{
				free(temp_buffer);
				return 1;
			}
			length = br;
			if(length > 0)
			{
				wavplayer->player->set_buffer_valid(wav_buffer, 1);
				if(wavplayer->player->get_status() == PLAYER_STOP)
				{
					wavplayer->player->start();
				}
				wavplayer->player->delay_ms(1);
			}
		}while((length > 0 || length == wavplayer->player->buffer_size) && wavplayer->player->get_status() != PLAYER_STOP);
	}
	//
	//8bit 2 channel
	else if(wav_fmt.BitsPerSample == 8 && wav_fmt.NumChannels == 2)
	{
		do
		{
			wav_buffer = wavplayer->player->get_buffer_pos();
			if(wav_buffer == NULL)
			{
				wavplayer->player->delay_ms(1);
				continue;
			}
			//
			//merger 2channel
			for(int m = 0; m < wav_fmt.BlockAlign; m++)
			{
				res = f_read(&wav_file, temp_buffer, wavplayer->player->buffer_size, &br);
				if(res != FR_OK)
				{
					free(temp_buffer);
					f_close(&wav_file);
					return 1;
				}
				//
				//peak value  limit
				temp_length = br/wav_fmt.BlockAlign; 
				for(int n = 0; n < temp_length; n++)
				{
					wav_buffer[length+n] = (temp_buffer[n*2]+temp_buffer[n*2+1])/2;
				}
				length += temp_length;
			}
			//
			//set buffer valid or play start
			if(length > 0)
			{
				wavplayer->player->set_buffer_valid(wav_buffer, 1);
				if(wavplayer->player->get_status() == PLAYER_STOP)
				{
					wavplayer->player->start();
				}
				wavplayer->player->delay_ms(1);
			}
		}while((length > 0 || length == wavplayer->player->buffer_size/wav_fmt.BlockAlign) && wavplayer->player->get_status() != PLAYER_STOP);
	}
	//
	//16bit 1 channel
	else if(wav_fmt.BitsPerSample == 16 && wav_fmt.NumChannels == 1)
	{
		do
		{
			wav_buffer = wavplayer->player->get_buffer_pos();
			if(wav_buffer == NULL)
			{
				wavplayer->player->delay_ms(1);
				continue;
			}
			length = 0;
			temp_length = 0;
			
			for(int m = 0; m < wav_fmt.BlockAlign; m++)
			{
				res = f_read(&wav_file, temp_buffer, wavplayer->player->buffer_size, &br);
				if(res != FR_OK)
				{
					free(temp_buffer);
					f_close(&wav_file);
					return 1;
				}
				//
				//peak value  limit
				temp_length = br/wav_fmt.BlockAlign; 
				for(int n = 0; n < temp_length; n++)
				{
					wav_buffer[length+n] = (((int16_t*)temp_buffer)[n]>>8)+127;
				}
				length += temp_length;
			}
			//
			//set buffer valid or play start
			if(length > 0)
			{
				wavplayer->player->set_buffer_valid(wav_buffer, 1);
				if(wavplayer->player->get_status() == PLAYER_STOP)
				{
					wavplayer->player->start();
				}
				//wavplayer->player->delay_ms(1);
			}
		}while((length > 0 || length == wavplayer->player->buffer_size/wav_fmt.BlockAlign) && wavplayer->player->get_status() != PLAYER_STOP);
	}
	//
	//16bits 2channel
	else if(wav_fmt.BitsPerSample == 16 && wav_fmt.NumChannels == 2)
	{
		do
		{
			wav_buffer = wavplayer->player->get_buffer_pos();
			if(wav_buffer == NULL)
			{
				wavplayer->player->delay_ms(1);
				continue;
			}
			length = 0;
			temp_length = 0;
			//
			//
			for(int m = 0; m < wav_fmt.BlockAlign; m++)
			{
				res = f_read(&wav_file, temp_buffer, wavplayer->player->buffer_size, &br);
				if(res != FR_OK)
				{
					free(temp_buffer);
					f_close(&wav_file);
					return 1;
				}
				//
				//peak value  limit
				temp_length = br/wav_fmt.BlockAlign; 
				for(int n = 0; n < temp_length; n++)
				{
					wav_buffer[length+n] = (((((int16_t*)temp_buffer)[n*2]+((int16_t*)temp_buffer)[n*2+1])/2)>>8)+127;
				}
				length += temp_length;
			}
			//
			//set buffer valid or play start
			if(length > 0)
			{
				wavplayer->player->set_buffer_valid(wav_buffer, 1);
				if(wavplayer->player->get_status() == PLAYER_STOP)
				{
					wavplayer->player->start();
				}
				wavplayer->player->delay_ms(1);
			}
		}while((length > 0 || length == wavplayer->player->buffer_size/wav_fmt.BlockAlign) && wavplayer->player->get_status() != PLAYER_STOP);
	}
	free(temp_buffer);
	f_close(&wav_file);
	return 0;
}
/*********************************************
 * 
 * player set  shutdown state
 * 
 * ******************************************/
void wavplayer_shutdown(uint8_t sd)
{
	wavplayer->player->shutdown(sd);
}

/*********************************************
 * 
 * recorder start
 * 
 * ******************************************/
uint8_t wavplayer_recorder_start(char* path)
{
	
	FRESULT  res;
	FIL      wav_file;
	UINT     bw;
	uint16_t*     wav_buffer;
	uint32_t     wav_offset;
	wav_format_t wav_fmt;
	uint32_t     length;
	uint16_t*  temp_buffer;
	
	temp_buffer = malloc(wavplayer->recorder->buffer_size);
	res = f_open(&wav_file, path, FA_CREATE_NEW | FA_WRITE);
	
	if((res != FR_OK) && (wavplayer->recorder->get_status() == RECORDER_RECORDING))
	{
		free(temp_buffer);
		return 1;
	}
	//
	//set frame info
	
	//
	//write data
	do
	{
		wav_buffer = wavplayer->recorder->get_buffer_pos();
		if(wav_buffer == NULL)
		{
			wavplayer->player->delay_ms(1);
			continue;
		}
		//memcpy(temp_buffer,wav_buffer,wavplayer->recorder->buffer_size);
		res = f_write(&wav_file, wav_buffer, wavplayer->recorder->buffer_size, &bw);
		if(res != FR_OK)
		{
			free(temp_buffer);
			return 1;
		}
	}while(1);
}