#ifndef __RECORDER_ADC_H__
#define __RECORDER_ADC_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "stm32h7xx_hal.h"
#include "recorder_dev.h"

/*****************************************************
 * 
 * STM32 API and definition
 * 
 ****************************************************/
//
//os definition if used os
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#define RECORDER_DELAY_MS         (void*)osDelay
//
//dac definition 
extern  ADC_HandleTypeDef         hadc1;
#define RECORDER_ADC_HANDLER      &hadc1
//
//1 ADC normally has multiple channel,
//for stm32, adc ch5 should set (5-1)
#define RECORDER_ADC_POS          4
#define RECORDER_CH_LEFT          ADC_CHANNEL_5
//
//timer definition
extern  TIM_HandleTypeDef         htim4;
#define RECORDER_TIMER_HANDLE     &htim4
#define RECORDER_TIMER_FREQ       240000000
#define RECORDER_TIMER_PRESCALER  1


/*****************************************************
 * 
 * dac dev  definition
 * 
 ****************************************************/
typedef struct
{
	//buffer
	uint16_t    buffer0[RECORDER_BUFFER_SIZE];
	uint16_t    buffer1[RECORDER_BUFFER_SIZE];
	//buffer pointer
	uint16_t*   (buffer_switch[2]);
	//buffer calid status
	uint8_t     buffer_valid[2];
	//bufer 0 or 1 index
	uint8_t     buffer_index;
	//status
	recorder_dev_status_t status;

}recorder_adc_dev_t;

/*****************************************************
 * 
 * recorder adc device init
 * 
 ****************************************************/
void recorder_dev_adc_init(recorder_dev_t* dev);
void recorder_adc_irq_conv_complete(void);

#ifdef __cplusplus
}
#endif

#endif

