#include "psram.h"
#include "string.h"

static void LY68L_SPI_CS_ENABLE(void);
static void LY68L_SPI_CS_DISABLE(void);

void        LY68L_SPI_Send_Byte(uint8_t byte);
uint8_t     LY68L_SPI_Receive_Byte(void);

/*********************************************************************
CS驱动函数


*********************************************************************/
static void LY68L_SPI_CS_ENABLE(void)
{
	HAL_GPIO_WritePin(LY68L_CS_GPIO_Port,LY68L_CS_Pin, GPIO_PIN_RESET);
}

static void LY68L_SPI_CS_DISABLE(void)
{
	HAL_GPIO_WritePin(LY68L_CS_GPIO_Port,LY68L_CS_Pin, GPIO_PIN_SET);
}

/*********************************************************************
发送单个字节


*********************************************************************/
void LY68L_SPI_Send_Byte(uint8_t byte)
{
	HAL_SPI_Transmit(&phspi,&byte,1,0xffff);
}
/*********************************************************************
接收单个字节


*********************************************************************/
uint8_t LY68L_SPI_Receive_Byte(void)
{
	uint8_t data = 0xFF;
	HAL_SPI_Receive(&phspi,&data,1,0xffff);
	return data;
}
/*********************************************************************
接收多个字节
return status 1 as ok,0 as fail
*********************************************************************/
uint8_t LY68L_SPI_Receive_Bytes(uint8_t* buffer, uint32_t length)
{
	return !HAL_SPI_Receive(&phspi,buffer,length,0xffff);
}
/*********************************************************************
发送多个字节
return status 1 as ok,0 as fail
*********************************************************************/
uint8_t LY68L_SPI_Send_Bytes(uint8_t* buffer, uint32_t length)
{
	return !HAL_SPI_Transmit(&phspi,buffer,length,0xffff);
}
/*********************************************************************
复位
*********************************************************************/
void LY68L_SPI_Reset(void)
{
  LY68L_SPI_CS_ENABLE();
  LY68L_SPI_Send_Byte(LY68L_ResetEnable);
	LY68L_SPI_Send_Byte(LY68L_Reset);
  LY68L_SPI_CS_DISABLE();
}
/*********************************************************************
读取芯片ID

*********************************************************************/
uint32_t MFID_KGD = 0;
uint64_t EID = 0;

uint32_t LY68L_SPI_ReadID(void)
{
  /* 选择串行FLASH: CS低电平 */
  LY68L_SPI_CS_ENABLE();
  /* 发送命令：读取芯片型号ID */
  LY68L_SPI_Send_Byte(LY68L_ReadID);
	LY68L_SPI_Send_Byte(0x00);
	LY68L_SPI_Send_Byte(0x00);
	LY68L_SPI_Send_Byte(0x00);
  MFID_KGD |= ((uint32_t)LY68L_SPI_Receive_Byte())<<8;
  MFID_KGD |= (uint32_t)LY68L_SPI_Receive_Byte();
	
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<48;
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<40;
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<32;
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<24;
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<16;
  EID |= ((uint64_t)LY68L_SPI_Receive_Byte())<<8;
  EID |= (uint64_t)LY68L_SPI_Receive_Byte();
	
  LY68L_SPI_CS_DISABLE();
  return MFID_KGD;
}

/*********************************************************************
初始化


*********************************************************************/
uint8_t LY68L_SPI_Init(void)
{
	LY68L_SPI_Reset();
	if(LY68L_SPI_ReadID() == ((((uint32_t)(LY68L_MFID))<<8) + (LY68L_KGD_Pass)))
	{
		return 1;
	}
	else return 0;
}


/*********************************************************************
PSRAM read



*********************************************************************/

uint8_t LY68L_SPI_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t Length)   
{ 
 	uint8_t status;  										    
	LY68L_SPI_CS_ENABLE();                          //使能器件   
	LY68L_SPI_Send_Byte(LY68L_Read);      //发送读取命令  
	LY68L_SPI_Send_Byte((uint8_t)((ReadAddr)>>16));   //发送24bit地址    
	LY68L_SPI_Send_Byte((uint8_t)((ReadAddr)>>8));   
	LY68L_SPI_Send_Byte((uint8_t)ReadAddr); 
	status = LY68L_SPI_Receive_Bytes(pBuffer,Length);  
	LY68L_SPI_CS_DISABLE();  		
	return status;	
}  
/*********************************************************************
PSRAM fast read

*********************************************************************/
uint8_t LY68L_SPI_FastRead(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t Length)   
{ 
 	uint8_t status;   										    
	LY68L_SPI_CS_ENABLE();                          //使能器件   
	LY68L_SPI_Send_Byte(LY68L_FastRead);      //发送读取命令  
	LY68L_SPI_Send_Byte((uint8_t)((ReadAddr)>>16));   //发送24bit地址    
	LY68L_SPI_Send_Byte((uint8_t)((ReadAddr)>>8));   
	LY68L_SPI_Send_Byte((uint8_t)ReadAddr);   
	LY68L_SPI_Send_Byte(0x00);  //wait cycle 8cycles
	status = LY68L_SPI_Receive_Bytes(pBuffer,Length);
	LY68L_SPI_CS_DISABLE();  		
	return status;	
}  

/*********************************************************************
PSRAM write

only for freq low than 33MHz
*********************************************************************/

uint8_t LY68L_SPI_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t Length)   
{ 
 	uint8_t status; 										    
	LY68L_SPI_CS_ENABLE();                          //使能器件   
	LY68L_SPI_Send_Byte(LY68L_Write);      //发送读取命令  
	LY68L_SPI_Send_Byte((uint8_t)((WriteAddr)>>16));   //发送24bit地址    
	LY68L_SPI_Send_Byte((uint8_t)((WriteAddr)>>8));   
	LY68L_SPI_Send_Byte((uint8_t)WriteAddr);  
	status = LY68L_SPI_Send_Bytes(pBuffer,Length);
	LY68L_SPI_CS_DISABLE();  		
	return status;	
} 
/*********************************************************************
PSRAM test api

*********************************************************************/
uint8_t LY68L_SPI_Test(void)   
{ 
	char testbuffer[]={"123test123"};
	char testlength = strlen(testbuffer);
	LY68L_SPI_Write((uint8_t*)testbuffer,0,testlength);
	memset(testbuffer,0,sizeof(testbuffer));
	LY68L_SPI_FastRead((uint8_t*)testbuffer,0,testlength);
	if(strstr(testbuffer,"test")!=NULL)
	{
		return 1;
	}
	return 0;
}

